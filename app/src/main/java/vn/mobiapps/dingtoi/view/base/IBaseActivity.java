package vn.mobiapps.dingtoi.view.base;

/**
 * Created by Truong Thien on 10/26/2017.
 */

public interface IBaseActivity extends IBaseView{
    void showProgressBar();
    void hideProgressBar();
}
