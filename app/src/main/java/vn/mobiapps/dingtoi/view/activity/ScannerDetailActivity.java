package vn.mobiapps.dingtoi.view.activity;

import android.os.Bundle;

import vn.mobiapps.dingtoi.R;
import vn.mobiapps.dingtoi.utils.Constant;
import vn.mobiapps.dingtoi.view.base.BaseActivity;
import vn.mobiapps.dingtoi.view.fragment.scandetail.ScannerDetailFragment;

public class ScannerDetailActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_detail);
        Constant.context = this;
        pushFragment(new ScannerDetailFragment());
    }

    @Override
    public void onBackPressed() {
        popFragment();
    }

    @Override
    public void endFragment() {
        this.finish();
    }
}
