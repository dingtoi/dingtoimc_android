package vn.mobiapps.dingtoi.view.base;

import android.os.Bundle;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import vn.mobiapps.dingtoi.R;

public abstract class TabbarActivity extends BaseActivity {
    public FrameLayout layout_content;
    public LinearLayout layout_tabbar;

    protected abstract int selectedItemTabbar();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbar);
        layout_content = findViewById(R.id.layout_content);
        layout_tabbar = findViewById(R.id.layout_tabbar);
    }
}
