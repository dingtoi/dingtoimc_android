package vn.mobiapps.dingtoi.view.fragment.start;

import vn.mobiapps.dingtoi.view.base.IBaseView;

public interface IInputTransactionView extends IBaseView {
    void checkTransactionCode();
}
