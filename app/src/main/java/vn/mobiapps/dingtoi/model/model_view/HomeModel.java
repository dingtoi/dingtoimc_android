package vn.mobiapps.dingtoi.model.model_view;

import java.io.Serializable;

public class HomeModel implements Serializable {
    public String id;
    public int img;
    public String content;
    public String link;

    public HomeModel(String id, int img, String content, String link) {
        this.id = id;
        this.img = img;
        this.content = content;
        this.link = link;
    }
}
