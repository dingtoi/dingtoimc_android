package vn.mobiapps.dingtoi.model.model_view;

import java.io.Serializable;

/**
 * Created by TRANVIETTHUC on 27/10/2017.
 */

public class ResponseErrorModel implements Serializable {
    public String status;
    public String code;
    public String messenge;

    public ResponseErrorModel() {
    }

    public ResponseErrorModel(String status, String code, String messenge) {
        this.status = status;
        this.code = code;
        this.messenge = messenge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessenge() {
        return messenge;
    }

    public void setMessenge(String messenge) {
        this.messenge = messenge;
    }
}
