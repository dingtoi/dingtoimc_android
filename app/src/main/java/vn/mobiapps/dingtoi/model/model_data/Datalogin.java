package vn.mobiapps.dingtoi.model.model_data;

import java.io.Serializable;

public class Datalogin implements Serializable {
    public String staff_user_id;
    public String staff_full_name;
    public String staff_logged_in;
    public String cookie;
}
