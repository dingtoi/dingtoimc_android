package vn.mobiapps.dingtoi.model.model_view;

import java.io.Serializable;

public class LoginRequest implements Serializable {
    public String phone_number;
    public String password;

    public LoginRequest(String phone_number, String password) {
        this.phone_number = phone_number;
        this.password = password;
    }
}
