package vn.mobiapps.dingtoi.apimanager;


import vn.mobiapps.dingtoi.model.model_view.ResponseErrorModel;

/**
 * Created by Truong Thien on 1/17/2019.
 */

public class APIInterface {
    public interface onDelegate{
        void onSuccess(Object result);
        void onError(ResponseErrorModel error);
    }
}
