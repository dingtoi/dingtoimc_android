package vn.mobiapps.dingtoi.presenter.scandetail;

import android.content.Context;

import java.util.ArrayList;

import vn.mobiapps.dingtoi.interfaces.IBaseInterface;
import vn.mobiapps.dingtoi.model.model_view.LogModel;

public interface IScanDetailPresenter extends IBaseInterface {
    boolean hasBackCamera();
    boolean isBluetoothSupported();
    boolean isHasFingterprinter();
    boolean isWifiSupported();
    int getTotalInternalMemorySize();
    String latestVersion();
    String hardware();
    Double getFreeDiskStorage();
    boolean isFlashSupported();
    boolean hasTelephony();
    boolean hasGsmOrCdma();
    boolean hasControlVolume();
    boolean hasAudioOutput();
    String getDeviceName();
    int diamondRating(int pointPhysicalGrading);
    void writeLogFile(ArrayList<LogModel> listDataLog);
    String getTelephoneNumber();
}
