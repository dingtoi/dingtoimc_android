package vn.mobiapps.dingtoi.interactor.scandetail;

import vn.mobiapps.dingtoi.apimanager.APIInterface;
import vn.mobiapps.dingtoi.apimanager.APIManager;
import vn.mobiapps.dingtoi.interfaces.BaseInterfaceImpl;
import vn.mobiapps.dingtoi.interfaces.IBaseListener;
import vn.mobiapps.dingtoi.model.model_data.PhoneModel;
import vn.mobiapps.dingtoi.model.model_view.LoginRequest;
import vn.mobiapps.dingtoi.model.model_view.ResponseErrorModel;

/**
 * Created by Truong Thien on 4/19/2019.
 */

public class ScanDetailInteractorImpl extends BaseInterfaceImpl implements IScanDetailInteractor {

    private IBaseListener interactorListener;

    public ScanDetailInteractorImpl(IBaseListener interactorListener ) {
        if (interactorListener != null) {
            this.interactorListener = interactorListener;
        }
    }

    @Override
    public void login(LoginRequest loginRequest) {
        APIManager.Authentication_Login(loginRequest, new APIInterface.onDelegate() {
            @Override
            public void onSuccess(Object result) {
                interactorListener.onSuccess(result);
            }

            @Override
            public void onError(ResponseErrorModel error) {
                interactorListener.onError(error);
            }
        });
    }

    @Override
    public void checkTransactionCode(String transactionCode) {
        APIManager.checkTransactionCode(transactionCode, new APIInterface.onDelegate() {
            @Override
            public void onSuccess(Object result) {
                interactorListener.onSuccess(result);
            }

            @Override
            public void onError(ResponseErrorModel error) {
                interactorListener.onError(error);
            }
        });
    }

    @Override
    public void checkTextInbound(PhoneModel phoneModel) {
        APIManager.checkTextInbound(phoneModel, new APIInterface.onDelegate() {
            @Override
            public void onSuccess(Object result) {
                interactorListener.onSuccess(result);
            }

            @Override
            public void onError(ResponseErrorModel error) {
                interactorListener.onError(error);
            }
        });
    }

    @Override
    public void checkVoiceInbound(PhoneModel phoneModel) {
        APIManager.checkVoiceInbound(phoneModel, new APIInterface.onDelegate() {
            @Override
            public void onSuccess(Object result) {
                interactorListener.onSuccess(result);
            }

            @Override
            public void onError(ResponseErrorModel error) {
                interactorListener.onError(error);
            }
        });
    }
}
