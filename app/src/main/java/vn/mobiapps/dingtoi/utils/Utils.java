package vn.mobiapps.dingtoi.utils;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

public class Utils {
    public static CountDownTimer countDownTimer = null;
    public static int t;

    public static byte[] encodeParameters(Map<String, String> params, String paramsEncoding) {
        StringBuilder encodedParams = new StringBuilder();
        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                encodedParams.append(URLEncoder.encode(entry.getKey(), paramsEncoding));
                encodedParams.append('=');
                encodedParams.append(URLEncoder.encode(entry.getValue(), paramsEncoding));
                encodedParams.append('&');
            }
            return encodedParams.toString().getBytes(paramsEncoding);
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Encoding not supported: " + paramsEncoding, uee);
        }
    }

    public static String formatDateTimeZOne(String dateTime) {
        TimeZone tz = TimeZone.getTimeZone("GMT");
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS"); //set time có múi giờ
        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ"); //set time có múi giờ
        //DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        DateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
        //DateFormat format2 = new SimpleDateFormat("HH:mm:ss");
        DateFormat format2 = new SimpleDateFormat("HH:mm");
        df.setTimeZone(tz);
        String day = "", time = "";
        try {
            day = format1.format(df.parse(dateTime));
            time = format2.format(df.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return day + "    " + time;
    }

    public static String formatDateTime(String dateTime) {
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        DateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
        String day = "";
        try {
            day = format1.format(df.parse(dateTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return day;
    }

    public static int numberOfDaysInMonth(int month, int year) {
        Calendar monthStart = new GregorianCalendar(year, month, 1);
        return monthStart.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    public static void CallPhones(Context context, String number) {
        Intent intent0 = new Intent(Intent.ACTION_DIAL);
        intent0.setData(Uri.parse("tel:" + number));
        context.startActivity(intent0);
    }

    public static void sendMesseger(Context context, String number) {
        Intent intent1 = new Intent();
        intent1.setAction(Intent.ACTION_SENDTO);
        intent1.putExtra("sms_body", "");
        intent1.setType("text/plain");
        intent1.setData(Uri.parse("sms:" + number));
        context.startActivity(intent1);

    }

    public static void CallPhonebook(Context context, String name, String phone) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType(ContactsContract.Contacts.CONTENT_TYPE);
        intent.putExtra(ContactsContract.Intents.Insert.NAME, name);
        intent.putExtra(ContactsContract.Intents.Insert.PHONE, phone);
        context.startActivity(intent);
    }

    public static boolean setListViewHeightBasedOnItems(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {
            int numberOfItems = listAdapter.getCount();
            // Get total height of all items.
            int totalItemsHeight = 0;
            for (int itemPos = 0; itemPos < numberOfItems; itemPos++) {
                View item = listAdapter.getView(itemPos, null, listView);
                item.measure(0, 0);
                totalItemsHeight += item.getMeasuredHeight();
            }
            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() * (numberOfItems - 1);
            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();
            return true;
        } else {
            return false;
        }
    }

    public static void start() {
        if (Utils.countDownTimer != null) {
            countDownTimer.cancel();
        }
        countDownTimer = new CountDownTimer(3 * 1000, 1000) {
            @Override
            public void onTick(long l) {
                t = (int) (l / 1000);
            }

            @Override
            public void onFinish() {
            }
        };
        countDownTimer.start();
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static List<Date> getDates(String dateString1, String dateString2) {
        ArrayList<Date> dates = new ArrayList<Date>();
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = df1 .parse(dateString1);
            date2 = df1 .parse(dateString2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(date1);

        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(date2);

        while(!cal1.after(cal2)) {
            dates.add(cal1.getTime());
            cal1.add(Calendar.DATE, 1);
        }
        return dates;
    }

    public static String getCurrentDate() {
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH) + 1;
        int day = c.get(Calendar.DAY_OF_MONTH);
        String m = "", d = "";
        if (String.valueOf(month).length() == 1) {
            m = "0" + month;
        } else {
            m = month + "";
        }
        if (String.valueOf(day).length() == 1) {
            d = "0" + day;
        } else {
            d = day + "";
        }
        return year + "-" + m + "-" + d;
    }
}
