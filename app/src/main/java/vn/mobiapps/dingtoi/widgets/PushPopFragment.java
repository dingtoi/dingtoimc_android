package vn.mobiapps.dingtoi.widgets;

import android.annotation.SuppressLint;
import android.app.Fragment;
import android.util.Log;

import androidx.fragment.app.FragmentTransaction;

import java.util.Iterator;
import java.util.List;

import vn.mobiapps.dingtoi.view.base.BaseActivity;
import vn.mobiapps.dingtoi.view.base.BaseFragment;

public class PushPopFragment extends Fragment {
    public static final String TAG = PushPopFragment.class.getSimpleName();

    private BaseActivity activity;
    private int IDContentReplace;

    public int getIDContentReplace() {
        return IDContentReplace;
    }

    public void setIDContentReplace(int IDContentReplace) {
        this.IDContentReplace = IDContentReplace;
    }

    /*Method pust Fragment not Animotion*/
    public void pushFragment(BaseFragment fragment, List<BaseFragment> listFragment, BaseActivity activity) {
        try {
            this.activity = activity;
            logBefor(true, listFragment);
            if (listFragment != null) {
                listFragment.add(fragment);
                activity.getSupportFragmentManager().beginTransaction()
                        .replace(getIDContentReplace(), fragment)
                        .commit();
            }
            logAfter(true, listFragment);
        } catch (Exception e) {
            Log.e("ERROR PUSH FRAGMENT : ", e.toString());
        }
    }

    /*Method pust Fragment Animotion*/
    public void pushFragmentAnimotion(BaseFragment fragment, List<BaseFragment> listFragment, BaseActivity activity) {
        try {
            this.activity = activity;
            logBefor(true, listFragment);
            if (listFragment != null) {
                listFragment.add(fragment);
                activity.getSupportFragmentManager().beginTransaction()
                        //.setCustomAnimations(R.anim.enter2, R.anim.exit2)
                        .replace(getIDContentReplace(), fragment)
                        .commit();
            }
            logAfter(true, listFragment);
        } catch (Exception e) {
            Log.e("ERROR PUSH FRAGMENT : ", e.toString());
        }
    }

    /*Method pop Fragment not Animotion*/
    public void popFragment(List<BaseFragment> listFragment, BaseActivity activity) {
        try {
            this.activity = activity;
            logBefor(false, listFragment);
            if (listFragment != null && listFragment.size() > 1) {
                BaseFragment fragment = listFragment.get(listFragment.size() - 2);
                if (fragment != null) {
                    FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(getIDContentReplace(), fragment);
                    transaction.commit();
                }
                listFragment.remove(listFragment.size() - 1).onLowMemory();
            } else {
                endFragment();
            }
            logAfter(false, listFragment);
        } catch (Exception e) {
            Log.e("ERROR POP FRAGMENT : ", e.getMessage());
        }
    }

    /*Method pop to index*/
    public void popToIndex(int foundIndex, List<BaseFragment> listFragment, BaseActivity activity) {
        try {
            logBefor(false, listFragment);
            if (listFragment != null) {
                boolean check = false;
                for (int i = listFragment.size() - 1; i >= 0; i--) {
                    if (i == (foundIndex)) {
                        check = true;
                        BaseFragment fragment = listFragment.get(i);
                        activity.getSupportFragmentManager().beginTransaction()
                                .replace(getIDContentReplace(), fragment)
                                .commit();
                        break;
                    } else {
                        listFragment.remove(i).onLowMemory();
                    }
                }

                if (!check) {
                    listFragment.removeAll(listFragment);
                    endFragment();
                }
            }
            logAfter(false, listFragment);
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    /*Search index Fragment in list Stack*/
    public int isExistFragment(String className, List<BaseFragment> listFragment) {
        int foundIndex = -1;
        try {
            if (className != null && listFragment != null) {
                Iterator<BaseFragment> cursor = listFragment.iterator();
                for (int i = 0; i < listFragment.size(); i++) {
                    if (cursor.next().getClass().getSimpleName().equalsIgnoreCase(className)) {
                        foundIndex = i;
                        break;
                    }
                }
            }
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
        return foundIndex;
    }

    /*Method Check list Befor push in srack*/
    @SuppressLint("LongLogTag")
    protected void logBefor(Boolean push, List<BaseFragment> listFragment) {
        if (push) {
            if (listFragment != null && listFragment.size() > 0) {
                Log.e(TAG + ">>>>>>>>>> Trước khi push có: ", listFragment.size() + "Fragment");
                Iterator<BaseFragment> cursor = listFragment.iterator();
                int i = 0;
                while (cursor.hasNext()) {
                    Log.e("index = " + i, cursor.next().getClass().getSimpleName());
                    i++;
                }
            } else {
                Log.e(TAG + ">>>>>>>>>> Trước khi push có: ", "0");
            }
        } else {
            if (listFragment != null && listFragment.size() > 0) {
                Log.e(TAG + "<<<<<<<<<<<<<<<<<<<< Trước khi pop có: ", listFragment.size() + "");
                Iterator<BaseFragment> cursor = listFragment.iterator();
                int i = 0;
                while (cursor.hasNext()) {
                    Log.e("index = " + i, cursor.next().getClass().getSimpleName());
                    i++;
                }
            } else {
                Log.e(TAG + "<<<<<<<<<<<<<<<<<<<<  Trước khi pop có: ", "0");
            }
        }
    }

    /*Method check List apter push in stack*/
    @SuppressLint("LongLogTag")
    protected void logAfter(Boolean push, List<BaseFragment> listFragment) {
        if (push) {
            if (listFragment != null && listFragment.size() > 0) {
                Log.e(TAG + ">>>>>>>>>> Sau khi push có: ", listFragment.size() + "");
                Iterator<BaseFragment> cursor = listFragment.iterator();
                int i = 0;
                while (cursor.hasNext()) {
                    Log.e("index = " + i, cursor.next().getClass().getSimpleName());
                    i++;
                }
            } else {
                Log.e(TAG + ">>>>>>>>>> Sau khi push có: ", "0");
            }
        } else {
            if (listFragment != null && listFragment.size() > 0) {
                Log.e(TAG + "<<<<<<<<<<<<<<<<<< Sau khi pop có: ", listFragment.size() + "");
                Iterator<BaseFragment> cursor = listFragment.iterator();
                int i = 0;
                while (cursor.hasNext()) {
                    Log.e("index = " + i, cursor.next().getClass().getSimpleName());
                    i++;
                }
            } else {
                Log.e(TAG + "<<<<<<<<<<<<<<<<<<< Sau khi pop có: ", "0");
            }
        }
    }

    /*Method active finish Activity*/
    public void endFragment() {
        this.activity.endFragment();
    }
}
