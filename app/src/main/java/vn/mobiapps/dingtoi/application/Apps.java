package vn.mobiapps.dingtoi.application;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;
import android.util.Log;

import androidx.multidex.MultiDex;

public class Apps extends Application {
    private String TAG = Apps.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        getDeviceID();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    private void getDeviceID() {
        String deviceID = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e(TAG, "DeviceID: >>>" + deviceID);
    }
}
